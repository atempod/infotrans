$(document).ready(function() {
  var $navbar = $("#mNavbar");

  AdjustHeader(); // For special case (to loadthe page from halfway down, etc);
  $(window).scroll(function() {
      AdjustHeader();
  });

  function AdjustHeader(){
    if ($(window).scrollTop() > 50) {
      if (!$navbar.hasClass("fixed-top")) {
        $navbar.addClass("fixed-top");
        // $("header").addClass("top-shift");
        $("#navbarResponsive").addClass("fixed-top");
        $("#escudo_nav").removeClass("invissible");
      }
    } else {
        $navbar.removeClass("fixed-top");
        // $("header").removeClass("top-shift");
        $("#navbarResponsive").removeClass("fixed-top");
        $("#escudo_nav").addClass("invissible");
    }
  }
});



