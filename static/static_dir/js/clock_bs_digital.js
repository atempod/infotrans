xtag.register('x-clock', {
 lifecycle: {
 created: function(){
 this.start();
 }
 },
 methods: {
 start: function(){
 this.update();
 this.xtag.interval = setInterval(this.update.bind(this), 1000);
 },
 stop: function(){
 this.xtag.interval = clearInterval(this.xtag.interval);
 },
 update: function(){
 this.textContent = new Date().toLocaleTimeString('en-GB');
 }
 },
 events: {
 tap: function(){
 if (this.xtag.interval) this.stop();
 else this.start();
 }
 }
});

xtag.register('x-date', {
 lifecycle: {
 created: function(){
 this.start();
 }
 },
 methods: {
 start: function(){
 this.update();
 this.xtag.interval = setInterval(this.update.bind(this), 60000);
 },
 stop: function(){
 this.xtag.interval = clearInterval(this.xtag.interval);
 },
 update: function(){
 this.textContent = new Date().toDateString();
 }
 },
 events: {
 tap: function(){
 if (this.xtag.interval) this.stop();
 else this.start();
 }
 }
});


