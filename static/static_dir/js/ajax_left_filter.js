
// ====================  Preset form values for "#schedule_from"/"#schedule_to" cases  =========
$(function(){
  //  Preset form values for "#schedule_from" case
  $("#schedule_from").click(function(){
       $("#id_show_from").val('Cuenca').change();
       $("#id_show_to").val('any').change();
       $('input[name="company_choices"]').prop('checked', true).change();
    });
});


$(function(){
  //  Preset form values for "#schedule_to" case
  $("#schedule_to").click(function(){
       $("#id_show_from").val('any').change();
       $("#id_show_to").val('Cuenca').change();
       $('input[name="company_choices"]').prop('checked', true).change();
  });
});

// ==================== end of Preset form values for "#schedule_from"/"#schedule_to" cases ========================



// ======== AJAX change div with template -- all OK  =====
$(function(){
    // Change "#schedule_table" div data with "/ajax_schedule_data" template
    $("#id_show_from, #id_show_to, #id_company_choices").change(function(){
        var show_from = $(id_show_from).val();
        var show_to = $(id_show_to).val();
        var checkedCompanies = $('input[name="company_choices"]:checked').map(function() {
            return this.value;
        }).get().join(",");

        $("#schedule_table").load("/ajax_schedule_data/", {
                                'show_from': show_from,
                                'show_to': show_to,
                                'checkedCompanies': checkedCompanies
        });
    });
});


// ======== testing AJAX data transfer -- all OK  =====
$(function(){
  //  Change "#ajax_schedule_data" in show_from/show_to with "/ajax_schedule_data" template
  $("#ajax_schedule_data").click(function(){
      show_from = $(id_show_from).val();
      show_to = $(id_show_to).val();

      $("#schedule_table").load("/ajax_schedule_data/", {
                                'show_from': show_from,
                                'show_to': show_to
      });
  });
});
