from django.conf.urls import url
# from django.views.generic imports ListView
from . import views


urlpatterns = [
    url(r'^$', views.organizer, name='organizer'),
    url(r'^company/create/$', views.company_create,
        name='company_create'),
    url(r'^company/update/(?P<pk>[0-9]+)/$', views.company_update,
        name='company_update'),
    url(r'^company/delete/(?P<pk>[0-9]+)/$', views.company_delete,
        name='company_delete'),

    url(r'^company/routes/organizer/(?P<pk>[0-9]+)/$',
        views.company_routes_organizer, name='company_routes_organizer'),
    url(r'^company/route/create/(?P<pk>[0-9]+)/$',
        views.company_route_create, name='company_route_create'),
    url(r'^company/route/update/(?P<pk>[0-9]+)/$',
        views.company_route_update, name='company_route_update'),
    url(r'^company/route/delete/(?P<pk>[0-9]+)/$', views.route_delete,
        name='route_delete'),

    url(r'^route/trips/organizer/(?P<pk>[0-9]+)/$',
        views.route_trips_organizer, name='route_trips_organizer'),
    url(r'^route/trip/create/(?P<pk>[0-9]+)/$',
        views.route_trip_create, name='route_trip_create'),
    url(r'^route/trip/update/(?P<pk>[0-9]+)/$',
        views.route_trip_update, name='route_trip_update'),
    url(r'^route/trip/delete/(?P<pk>[0-9]+)/$', views.trip_delete,
        name='trip_delete'),

    url(r'^trip/stops/organizer/(?P<pk>[0-9]+)/$',
        views.trip_stops_organizer, name='trip_stops_organizer'),
    url(r'^trip/stop/create/(?P<pk>[0-9]+)/$',
        views.trip_stop_create, name='trip_stop_create'),
    url(r'^trip/stop/update/(?P<pk>[0-9]+)/$',
        views.trip_stop_update, name='trip_stop_update'),
    url(r'^trip/stop/delete/(?P<pk>[0-9]+)/$', views.stop_delete,
        name='stop_delete'),

]

