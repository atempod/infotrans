## -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json

from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import ObjectDoesNotExist

# from django.views.decorators.csrf imports csrf_exempt
# from django.views imports generic

from itrans.models import Company, Route, Trip, Stop
from itrans.forms import FilterForm, BackMessageForm, FORM_COMPANIES
from .forms import OrgCompanyForm, OrgRouteForm, OrgTripForm, OrgStopForm
from core_utils.forms.utils import form_errors_to_json

all_companies = sorted([comp[0] for comp in FORM_COMPANIES])
# order options
order_to_list = ['route__route_finish__name', 'start_time', 'route__company__name']
order_from_list = ['route__route_start__name', 'start_time', 'route__company__name']
order_by_company_list = ['route__company','route__route_start__name',
                         'route__route_finish__name', 'start_time']


@user_passes_test(lambda u: u.is_superuser)
def organizer(request):
    """ Start page of custom admin interface """
    template = 'organizer/admin_start.html'
    companies = Company.objects.all().order_by('name')
    routes = Route.objects.all().order_by('route_name', 'company__name')
    trips = Trip.objects.all().order_by(*order_by_company_list)
    ctx = {
        'companies': companies,
        'routes': routes,
        'trips': trips
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def company_create(request):
    """ Create new company item """
    redirect_to = reverse('organizer')
    redirect_to_self = reverse('company_create')
    template = 'organizer/update_company_details.html'
    if request.method == "POST":
        form = OrgCompanyForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        form = OrgCompanyForm()
    ctx = {
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def company_update(request, pk):
    """ Update chosen company details """
    redirect_to = reverse('organizer')
    redirect_to_self = reverse('company_update', kwargs={'pk':pk})
    company = get_object_or_404(Company, pk=pk)
    template = 'organizer/update_company_details.html'
    if request.method == "POST":
        pass
        form = OrgCompanyForm(instance=company, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'name': company.name,
            'full_name': company.full_name,
            'code': company.code,
            'prename': company.prename,
            'is_terminal_terrestre': company.is_terminal_terrestre,
            'terminal': company.terminal,
            'terminal_address': company.terminal_address,
            'office_address': company.office_address,
            'office_opening': company.office_opening,
            'office_closing': company.office_closing,
            'terminal_map': company.terminal_map,
            'office_map': company.office_map,
            'website': company.website,
            'website2': company.website2,
            'email': company.email,
            'phone': company.phone,
            'phone2': company.phone2,
            'phone_tickets': company.phone_tickets,
            'phone_help': company.phone_help,
            'rating': company.rating,
            'comments': company.comments,
            'use_minibus': company.use_minibus,
            'description': company.description,
            'img_bus': company.img_bus,
            'img_bus2': company.img_bus2,
            'img_terminal': company.img_terminal,
        }
        form = OrgCompanyForm(initial=initial)
    ctx = {
        'company': company,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def company_delete(request, pk):
    """ Delete chosen company item """
    redirect_to = reverse('organizer')
    try:
        review_to_delete = Company.objects.get(pk=pk).delete()
    except ObjectDoesNotExist:
        print( "Try to delete the Company instance id =", pk ,"that doesn't exist.")
    return HttpResponseRedirect(redirect_to)


@user_passes_test(lambda u: u.is_superuser)
def company_routes_organizer(request, pk):
    """ Show and manage company routes """
    template = 'organizer/organize_company_routes.html'
    company = Company.objects.get(pk=pk)
    routes = Route.objects.filter(company=company).order_by(
                                        'to_end', 'stopover', 'stopover2')
    trips = Trip.objects.filter(route__company__name=company).order_by(
                                        *order_by_company_list)
    ctx = {
        'company': company,
        'routes': routes,
        'trips': trips
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def company_route_create(request, pk):
    """ Create new route item """
    redirect_to = reverse('company_routes_organizer', kwargs={'pk':pk})
    redirect_to_self = reverse('company_route_create', kwargs={'pk':pk})
    company = get_object_or_404(Company, pk=pk)
    template = 'organizer/update_company_route.html'
    if request.method == "POST":
        form = OrgRouteForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            print('after save')
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
        }
        form = OrgRouteForm(initial=initial)
    ctx = {
        'company': company,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def company_route_update(request, pk):
    """ Update chosen route details """
    redirect_to_self = reverse('company_route_update', kwargs={'pk':pk})
    route = get_object_or_404(Route, pk=pk)
    company = get_object_or_404(Company, pk=route.company.pk)
    redirect_to = reverse('company_routes_organizer', kwargs={'pk':route.company.pk})
    template = 'organizer/update_company_route.html'
    if request.method == "POST":
        form = OrgRouteForm(instance=route, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
            'route_name': route.route_name,
            'full_name': route.route_full_name,
            'route_brand_name': route.route_brand_name,
            'route_suffix': route.route_suffix,
            'code': route.code,
            'code_full': route.code_full,
            'from_end': route.from_end,
            'to_end': route.to_end,
            'stopover': route.stopover,
            'stopover2': route.stopover2,
            'route_pre_start': route.route_pre_start,
            'route_start': route.route_start,
            'route_finish': route.route_finish,
            'route_post_finish': route.route_post_finish,
            'via_city': route.via_city,
            'via_city2': route.via_city2,
            'is_express': route.is_express,
            'is_archived': route.is_archived,
            'price': route.price,
            'duration': route.duration,
            'full_time': route.full_time,
            'distance': route.distance,
            'bus_type': route.bus_type,
            'source': route.source,
            'description': route.description,
            'created_at': route.created_at,
            'updated_at': route.updated_at,
        }
        form = OrgRouteForm(initial=initial)
    ctx = {
        'company': company,
        'route': route,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def route_delete(request, pk):
    """ Delete chosen company item """
    try:
        route_to_delete = Route.objects.get(pk=pk)
        redirect_to = reverse('company_routes_organizer',
                              kwargs={'pk': route_to_delete.company.pk})
        route_to_delete.delete()
    except ObjectDoesNotExist:
        redirect_to = reverse('organizer')
        print( "Try to delete the Route instance id =", pk ,"that doesn't exist.")
    return HttpResponseRedirect(redirect_to)


@user_passes_test(lambda u: u.is_superuser)
def route_trips_organizer(request, pk):
    """ Show and manage route trips """
    template = 'organizer/organize_route_trips.html'
    route = Route.objects.get(pk=pk)
    company = route.company
    trips = Trip.objects.filter(route=route).order_by(
                                        *order_by_company_list)
    stops = Stop.objects.filter(trip__route__company__name=company).order_by(
                                        *order_by_company_list)
    ctx = {
        'company': route.company,
        'route': route,
        'trips': trips,
        'stops': stops
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def route_trip_create(request, pk):
    """ Create new trip item """
    redirect_to = reverse('route_trips_organizer', kwargs={'pk':pk})
    redirect_to_self = reverse('route_trip_create', kwargs={'pk':pk})
    route = get_object_or_404(Route, pk=pk)
    print('route =======', route)
    company = route.company
    template = 'organizer/update_route_trip.html'
    if request.method == "POST":
        form = OrgTripForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
            'route': route,
        }
        form = OrgTripForm(initial=initial)
    ctx = {
        'company': company,
        'route': route,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def route_trip_update(request, pk):
    """ Update chosen trip details """
    redirect_to_self = reverse('route_trip_update', kwargs={'pk':pk})
    trip = get_object_or_404(Trip, pk=pk)
    route = trip.route
    company = trip.route.company
    redirect_to = reverse('route_trips_organizer', kwargs={'pk':trip.route.pk})
    template = 'organizer/update_route_trip.html'
    if request.method == "POST":
        form = OrgTripForm(instance=trip, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
            'route': route,
            'code': trip.code,
            'code_from_service': trip.code_from_service,
            'code_from_company': trip.code_from_company,
            'name_full': trip.name_full,
            'name_brand': trip.name_brand,
            'start_time': trip.start_time,
            'days': trip.days,
            'is_archived': trip.is_archived,
            'is_draft': trip.is_draft,
            'notes': trip.notes,
            'created_at': trip.created_at,
            'updated_at': trip.updated_at,
        }
        form = OrgTripForm(initial=initial)
    ctx = {
        'company': company,
        'route': route,
        'trip': trip,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def trip_delete(request, pk):
    """ Delete chosen company item """
    try:
        trip_to_delete = Trip.objects.get(pk=pk)
        redirect_to = reverse('route_trips_organizer',
                              kwargs={'pk': trip_to_delete.route.pk})
        trip_to_delete.delete()
    except ObjectDoesNotExist:
        redirect_to = reverse('organizer')
        print( "Try to delete the Trip instance id =", pk ,"that doesn't exist.")
    return HttpResponseRedirect(redirect_to)


@user_passes_test(lambda u: u.is_superuser)
def trip_stops_organizer(request, pk):
    """ Show and manage trip stops """
    template = 'organizer/organize_trip_stops.html'
    trip = Trip.objects.get(pk=pk)
    stops = Stop.objects.filter(trip=trip).order_by('serial_in_route')
    route = trip.route
    company = trip.route.company
    ctx = {
        'company': company,
        'route': route,
        'trip': trip,
        'stops': stops

    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def trip_stop_create(request, pk):
    """ Create new stop item """
    redirect_to = reverse('trip_stops_organizer', kwargs={'pk':pk})
    redirect_to_self = reverse('trip_stop_create', kwargs={'pk':pk})
    trip = get_object_or_404(Trip, pk=pk)
    route = trip.route
    company = trip.route.company
    template = 'organizer/update_trip_stop.html'
    if request.method == "POST":
        form = OrgStopForm(data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
            'route': route,
            'trip': trip,
        }
        form = OrgStopForm(initial=initial)
    ctx = {
        'company': company,
        'route': route,
        'trip': trip,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def trip_stop_update(request, pk):
    """ Update chosen stop details """
    redirect_to_self = reverse('route_trip_update', kwargs={'pk':pk})
    stop = get_object_or_404(Stop, pk=pk)
    trip = stop.trip
    route = stop.trip.route
    company = stop.trip.route.company
    redirect_to = reverse('trip_stops_organizer', kwargs={'pk':stop.trip.pk})
    template = 'organizer/update_trip_stop.html'

    if request.method == "POST":
        form = OrgStopForm(instance=stop, data=request.POST, files=request.FILES)
        if form.is_valid():
            form.save()
            if request.is_ajax():
                return HttpResponse(json.dumps({'success': True, 'location': redirect_to_self}), content_type="application/json")
            else:
                return redirect(redirect_to)
        elif request.is_ajax():
            return HttpResponse(form_errors_to_json(form), content_type="application/json")
    else:
        initial = {
            'company': company,
            'route': route,
            'trip': trip,
            'station': stop.station,
            'name': stop.name,
            'stoptype': stop.stoptype,
            'serial_in_route': stop.serial_in_route,
            'is_stopping': stop.is_stopping,
            'arrival': stop.arrival,
            'departure': stop.departure,
            'duration': stop.duration,
            'time_from_start': stop.time_from_start,
            'dist_from_start': stop.dist_from_start,
            'notes': trip.notes,
            'created_at': trip.created_at,
            'updated_at': trip.updated_at,
        }
        form = OrgStopForm(initial=initial)
    ctx = {
        'company': company,
        'route': route,
        'trip': trip,
        'stop': stop,
        'form': form,
    }
    return render(request, template, ctx)


@user_passes_test(lambda u: u.is_superuser)
def stop_delete(request, pk):
    """ Delete chosen company item """
    try:
        stop_to_delete = Stop.objects.get(pk=pk)
        redirect_to = reverse('trip_stops_organizer',
                              kwargs={'pk': stop_to_delete.trip.pk})
        stop_to_delete.delete()
    except ObjectDoesNotExist:
        redirect_to = reverse('organizer')
        print( "Try to delete the Stop instance id =", pk ,"that doesn't exist.")
    return HttpResponseRedirect(redirect_to)
