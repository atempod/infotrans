# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms

from itrans.models import Company, Route, Trip, Stop

class OrgCompanyForm(forms.ModelForm):

    class Meta:
        model = Company
        exclude = ('',)


class OrgRouteForm(forms.ModelForm):

    class Meta:
        model = Route
        exclude = ('',)


class OrgTripForm(forms.ModelForm):

    class Meta:
        model = Trip
        exclude = ('',)


class OrgStopForm(forms.ModelForm):

    class Meta:
        model = Stop
        exclude = ('',)
