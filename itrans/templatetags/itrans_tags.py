from django import template
from itrans.models import Company, Route

register = template.Library()


# @register.assignment_tag
@register.simple_tag
def get_all_company_list_ordered_by_name():
    """ Tag prepare total company list ordered by company name """
    return Company.objects.all().order_by('name')


@register.simple_tag
def get_unique_children_count_tag(children_obj, parent_field, parent_id):
    """ Tag to count unique children count depend on parent object """
    children_count = children_obj.filter(
                            **{ parent_field: parent_id }).distinct().count()
    return children_count
