# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django import forms

from .models import Company, Route, Trip, BackMessage, City


try:
    companies_list = Company.objects.order_by('name').values_list('name').distinct()
    FORM_COMPANIES = tuple((item*2) for item in companies_list)
except Exception:
    print('Exception =', Exception)
    FORM_COMPANIES = ()
print('FORM_COMPANIES == ', FORM_COMPANIES)

try:
    cities_list = City.objects.order_by('name').values_list('name').distinct()
    ALL_CITIES = tuple((item*2) for item in cities_list)
    ALL_CITIES = ((u'todos', u'todos'),) + ALL_CITIES
except Exception:
    print('Exception =', Exception)
    ALL_CITIES = ()
print('ALL_CITIES == ', ALL_CITIES)

try:
    # from_list = Route.objects.all().order_by('from_end').values_list('from_end').distinct()
    from_list = Route.objects.all().order_by('route_start').values_list('route_start__name').distinct()
    FORM_FROM = tuple((item*2) for item in from_list)
    FORM_FROM = ((u'any', u'todos'),) + FORM_FROM
except Exception:
    print('Exception =', Exception)
    FORM_FROM = ()
print('FORM_FROM == ', FORM_FROM)

try:
    # to_list = Route.objects.all().order_by('to_end').values_list('to_end').distinct()
    to_list = Route.objects.all().order_by('route_finish').values_list('route_finish__name').distinct()
    FORM_TO = tuple((item*2) for item in to_list)
    FORM_TO = ((u'any', u'todos'),) + FORM_TO
except Exception:
    print('Exception =', Exception)
    FORM_TO = ('Cuenca', 'Loja', 'Quito')
print('FORM_TO == ', FORM_TO)


class FilterForm(forms.Form):
    """ Form for route filter """
    show_from = forms.ChoiceField(choices=FORM_FROM, label="Desde ciudad",
                                widget=forms.Select())
    show_to = forms.ChoiceField(choices=FORM_TO, label="Hasta cuidad",
                                widget=forms.Select())
    company_choices = forms.MultipleChoiceField(required=False,
        label = "Cooperativas", initial=True,
        choices = FORM_COMPANIES,
        widget = forms.CheckboxSelectMultiple)

    class Meta:
        fields = ('show_from', 'show_to', 'show_company')


class BackMessageForm(forms.ModelForm):
    """ Form for feedback message """
    # user = forms.CharField(label='Nombre de usuario', max_length=100,
    #                         help_text='Necesario. No más de 50 letras.')
    title = forms.CharField(label='Título de mensaje', max_length=100,
                            help_text='Necesario. No más de 100 letras.',
                            widget=forms.Textarea(attrs={'rows':3}))
    text = forms.CharField(label='Texto de mensaje', max_length=1000,
                            help_text='Necesario. No más de 1000 letras.',
                            widget=forms.Textarea)

    class Meta:
        model = BackMessage
        fields = ('title', 'text')

class CompanyForm(forms.ModelForm):

    class Meta:
        model = Company
        exclude = ('',)

class RouteForm(forms.ModelForm):

    class Meta:
        model = Route
        exclude = ('',)

class TripForm(forms.ModelForm):

    class Meta:
        model = Trip
        exclude = ('',)
