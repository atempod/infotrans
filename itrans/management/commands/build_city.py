# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from itrans.models import City, Canton, Province

from core_utils.imports import parse_excel


# Structure of excel table for cities (column names == model field names)
CITY_ATTR_TABLE = ['name', 'canton', 'province', 'status', 'code']
city_data_file = './data/city_data.xlsx'
cities_list = parse_excel.excel2dict_list(city_data_file, CITY_ATTR_TABLE)


class Command(BaseCommand):
    """
    Create/update list of cities, cantons and provincies.
    """

    def handle(self, *args, **options):

        for city_dict in cities_list:
            city_name = city_dict['name']
            print('city ===================================== ', city_name)

            # get or create Province
            province, __ = Province.objects.get_or_create(
                                            name=city_dict['province'])

            # update or create the Canton
            canton, __ = Canton.objects.update_or_create(
                                            name=city_dict['canton'],
                                            defaults={'province': province,})

            city_dict['province'] = province
            city_dict['canton'] = canton
            city_dict['status'] = city_dict['status'].lower()

            # update or create the City
            city, __ = City.objects.update_or_create(
                                            name=city_name,
                                            defaults=city_dict)
