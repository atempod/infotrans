# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from itrans.models import City, Canton, Province

from core_utils.imports import parse_excel


# Structure of excel table for provincies (column names == model field names)
PROVINCE_ATTR_TABLE = ['name', 'code', 'code_group']
province_data_file = './data/province_data.xlsx'
province_list = parse_excel.excel2dict_list(province_data_file, PROVINCE_ATTR_TABLE)


class Command(BaseCommand):
    """
    Create/update list of provincies.
    """

    def handle(self, *args, **options):

        for province_dict in province_list:
            province_name = province_dict['name']
            print('province ===================================== ',
                                                                province_name)

            # update or create the Province object
            province, __ = Province.objects.update_or_create(
                                                        name=province_name,
                                                        defaults=province_dict)

