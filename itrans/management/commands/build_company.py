# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from itrans.models import Company

from core_utils.imports import parse_excel


# Structure of excel table for companies (column names == model field names)
CONPANY_ATTR_TABLE = ['name', 'full_name', 'code', 'prename',
                      'is_terminal_terrestre', 'terminal', 'terminal_address',
                      'office_address', 'office_opening', 'office_closing',
                      'terminal_map', 'office_map', 'website', 'website2',
                      'email', 'phone', 'phone2', 'phone_tickets', 'phone_help',
                      'rating', 'comments', 'use_minibus', 'description']

company_data_file = './data/company_data.xlsx'
company_list = parse_excel.excel2dict_list(company_data_file, CONPANY_ATTR_TABLE)


class Command(BaseCommand):
    """
    Create/update list of provincies.
    """

    def handle(self, *args, **options):

        for company_dict in company_list:
            company_name = company_dict['name']
            print('company ===================================== ', company_name)

            # update or create Company object
            company, __ = Company.objects.update_or_create(name=company_name,
                                                           defaults=company_dict)
