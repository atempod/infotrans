# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django import forms
from .models import (Company, Route, BackMessage, SourceTrip,
                     Province, Canton, City, Station, RouteStation,
                     Trip, Stop,
                     )


class RouteStationAdmin(admin.ModelAdmin):
    # list_display = [f.name for f in RouteStation._meta.fields if f.name != 'id']
    # readonly_fields = ('route', 'arrival')
    list_filter = ('route',)
    ordering = (('code_full'), 'serial_in_route')

    list_display = (
                    'id',
                    'route',
                    'code_full',
                    'station',
                    # 'route_name',
                    'stoptype',
                    'arrival',
                    # 'arrival_formatted',
                    'departure',
                    # 'departure_formatted',
                    'duration',
                    'serial_in_route',
                    'time_from_start',
                    'dist_from_start',
                    'notes',)

    def departure_formatted(self, obj):
        """ Format the depature time in the table """
        if obj.departure:
            return obj.departure.strftime('%H:%M')
        else:
            return None

    def arrival_formatted(self, obj):
        """ Format the arrival time in the table """
        if obj.arrival:
            return obj.arrival.strftime('%H:%M')
        else:
            return None
    departure_formatted.admin_order_field = 'departure'
    departure_formatted.short_description = 'Departure'
    arrival_formatted.admin_order_field = 'arrival'
    arrival_formatted.short_description = 'Arrival'


class ProvinceAdmin(admin.ModelAdmin):

    list_display = [f.name for f in Province._meta.fields if f.name != 'id']
    list_filter = ('name',)
    ordering = ('name',)


class CantonAdmin(admin.ModelAdmin):

    list_display = [f.name for f in Canton._meta.fields if f.name != 'id']
    list_filter = ('name',)
    ordering = ('name',)


class CityAdmin(admin.ModelAdmin):

    list_display = [f.name for f in City._meta.fields if f.name != 'id']
    # list_filter = ('name',)
    ordering = ('name',)


class StationAdmin(admin.ModelAdmin):

    list_display = [f.name for f in Station._meta.fields if f.name != 'id']
    # list_filter = ('name',)
    ordering = ('name',)


class SourceTripAdmin(admin.ModelAdmin):

    list_display = [f.name for f in SourceTrip._meta.fields if f.name != 'id']
    # list_filter = ('name',)
    ordering = ('name',)


class CompanyAdmin(admin.ModelAdmin):

    list_display = [f.name for f in Company._meta.fields if f.name != 'id']
    # list_filter = ('name',)
    ordering = ('name',)


class RouteAdmin(admin.ModelAdmin):
    # def get_form(self, request, obj=None, **kwargs):
    #     # Fix proper form fields in add and edit forms
    #     if obj: # obj is not None, so this is for the change form
    #         kwargs['exclude'] = ['company', 'route_name', 'route_full_name',
    #                              'route_suffix', 'from_end', 'to_end',
    #                              'stopover', 'stopover2',
    #                              'route_start', 'route_finish', 'via_city',
    #                              'via_city2']
    #         kwargs['fields'] = ['company', 'route_full_name', 'code']
    #
    #     else: # obj is None, so this is for the add form
    #         pass
    #         kwargs['exclude'] = ['route_name', 'route_full_name',
    #                              'route_suffix', 'from_end', 'to_end',
    #                              'stopover', 'stopover2',]
    #         kwargs['fields'] = ['company', 'route_full_name', 'code']
    #
    #     return super(RouteAdmin, self).get_form(request, obj, **kwargs)

    # def get_form(self, *args, **kwargs):
    #
    #     form = super(RouteAdmin, self).get_form(*args, **kwargs)
    #
    #     for field_name in self.fake_readonly_fields:
    #         form.base_fields[field_name].widget.attrs["disabled"] = "disabled"
    #
    #     return form

    # readonly_fields = ('company', 'route_full_name',)
    readonly_fields = ('route_full_name',)
    list_display = [f.name for f in Route._meta.fields if (f.name != 'route_suffix' and f.name != 'from_end' and f.name != 'to_end' and f.name != 'stopover' and f.name != 'stopover2')]
    # list_display = [f.name for f in Route._meta.fields if (f.name != 'from_end' and f.name != 'to_end')]
    # list_display = [f.name for f in Route._meta.fields if (f.name != 'route_name' and f.name != 'route_full_name' and f.name != 'route_suffix' and f.name != 'from_end' and f.name != 'to_end' and f.name != 'stopover' and f.name != 'stopover2')]
    # list_display = ['route_name']
    list_filter = ('route_name',)
    # ordering = ('company', 'route_name',)
    ordering = ('code_full',)


class TripAdmin(admin.ModelAdmin):

    # list_display = ('route', 'start_time', 'code_from_service', 'code_from_company', 'name_full', 'name_brand', 'notes')
    list_display = [f.name for f in Trip._meta.fields if f.name != 'id']
    # list_display = ('route', 'start_time', 'code_from_service', 'code_from_company', 'notes')
    # list_filter = ('route', 'start_time', 'brand_name')
    ordering = ('route',)


class StopAdmin(admin.ModelAdmin):

    # list_display = ('route', 'start_time', 'code_from_service', 'code_from_company', 'brand_name', 'description')
    list_display = [f.name for f in Stop._meta.fields if f.name != 'id']
    list_filter = ('trip',)
    ordering = ('trip', 'serial_in_route')


class BackMessageAdmin(admin.ModelAdmin):

    list_display = ('user', 'title', 'text', 'created_at', 'updated_at')
    list_filter = ('user',)
    ordering = ('created_at', 'user', 'title')


admin.site.register(Province, ProvinceAdmin)
admin.site.register(Canton, CantonAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Station, StationAdmin)
admin.site.register(RouteStation, RouteStationAdmin)
admin.site.register(SourceTrip, SourceTripAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(Route, RouteAdmin)
admin.site.register(Stop, StopAdmin)
admin.site.register(Trip, TripAdmin)
admin.site.register(BackMessage, BackMessageAdmin)








