# Generated by Django 2.0 on 2018-01-31 10:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0041_auto_20180131_1235'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='StopTiming',
            new_name='Trip',
        ),
        migrations.AddField(
            model_name='route',
            name='from_end',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Departure city'),
        ),
        migrations.AddField(
            model_name='route',
            name='stopover',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Stopover'),
        ),
        migrations.AddField(
            model_name='route',
            name='stopover2',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Stopover 2'),
        ),
        migrations.AddField(
            model_name='route',
            name='to_end',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Arrival city'),
        ),
    ]
