# Generated by Django 2.0 on 2018-02-01 10:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0055_auto_20180201_1334'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='company',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AddField(
            model_name='routestation',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='routestation',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AddField(
            model_name='sourcetrip',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='sourcetrip',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AddField(
            model_name='station',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='station',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AddField(
            model_name='stop',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='stop',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
        migrations.AddField(
            model_name='trip',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='trip',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
    ]
