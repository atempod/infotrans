# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-06-27 10:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0016_company_is_terminal_terrestre'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='is_terminal_terrestre',
            field=models.BooleanField(default=True, verbose_name='Terminal Terrestre'),
        ),
    ]
