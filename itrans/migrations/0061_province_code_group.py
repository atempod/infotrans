# Generated by Django 2.0 on 2018-02-06 10:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0060_auto_20180203_0847'),
    ]

    operations = [
        migrations.AddField(
            model_name='province',
            name='code_group',
            field=models.CharField(blank=True, max_length=20, null=True, verbose_name='Code Group'),
        ),
    ]
