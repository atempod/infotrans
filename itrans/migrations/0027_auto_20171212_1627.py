# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-12-12 13:27
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0026_auto_20171209_1434'),
    ]

    operations = [
        migrations.CreateModel(
            name='SourceTrip',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='Name')),
                ('notes', models.CharField(blank=True, max_length=500, null=True, verbose_name='Name')),
            ],
        ),
        migrations.CreateModel(
            name='Station',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, unique=True, verbose_name='Name')),
                ('status', models.CharField(choices=[('unknown', 'Unknown'), ('city', 'In City'), ('village', 'In Village'), ('by_road', 'By the Road')], default='unknown', max_length=20, verbose_name='Status')),
                ('is_terminal', models.BooleanField(default=False, verbose_name='is a Terminal')),
                ('description', models.CharField(blank=True, max_length=500, null=True, verbose_name='Name')),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='itrans.City')),
            ],
            options={
                'verbose_name': 'Station',
                'verbose_name_plural': 'Stations',
            },
        ),
        migrations.AlterField(
            model_name='backmessage',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AlterField(
            model_name='backmessage',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
    ]
