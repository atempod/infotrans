# Generated by Django 2.0 on 2018-02-07 07:47

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0061_province_code_group'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='full_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Full Name'),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=50, unique=True, verbose_name='Name'),
        ),
    ]
