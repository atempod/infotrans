# -*- coding: utf-8 -*-
# Generated by Django 1.11.2 on 2017-07-31 11:38
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0018_auto_20170627_1120'),
    ]

    operations = [
        migrations.AlterField(
            model_name='route',
            name='route_name',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Route name'),
        ),
        migrations.AlterField(
            model_name='trip',
            name='code_from_company',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Code company'),
        ),
        migrations.AlterField(
            model_name='trip',
            name='code_from_service',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Code service'),
        ),
    ]
