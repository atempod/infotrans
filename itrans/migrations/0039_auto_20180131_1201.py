# Generated by Django 2.0 on 2018-01-31 09:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0038_auto_20180131_1147'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='route',
            name='from_end',
        ),
        migrations.RemoveField(
            model_name='route',
            name='stopover',
        ),
        migrations.RemoveField(
            model_name='route',
            name='stopover2',
        ),
        migrations.RemoveField(
            model_name='route',
            name='to_end',
        ),
        migrations.AddField(
            model_name='route',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True, verbose_name='Creation date'),
        ),
        migrations.AddField(
            model_name='route',
            name='source',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='itrans.SourceTrip'),
        ),
        migrations.AddField(
            model_name='route',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Update date'),
        ),
    ]
