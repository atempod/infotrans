# Generated by Django 2.0 on 2018-01-31 11:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('itrans', '0042_auto_20180131_1329'),
    ]

    operations = [
        migrations.AddField(
            model_name='trip',
            name='code',
            field=models.IntegerField(blank=True, default=1, null=True, verbose_name='Code'),
        ),
        migrations.AddField(
            model_name='trip',
            name='code_from_company',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Company code'),
        ),
        migrations.AddField(
            model_name='trip',
            name='code_from_service',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Service code'),
        ),
        migrations.AddField(
            model_name='trip',
            name='is_archived',
            field=models.BooleanField(default=False, verbose_name='Archived'),
        ),
        migrations.AddField(
            model_name='trip',
            name='is_draft',
            field=models.BooleanField(default=True, verbose_name='Draft'),
        ),
        migrations.AddField(
            model_name='trip',
            name='name_brand',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Brand name'),
        ),
        migrations.AddField(
            model_name='trip',
            name='name_full',
            field=models.CharField(blank=True, max_length=100, null=True, verbose_name='Full name'),
        ),
        migrations.AddField(
            model_name='trip',
            name='start_time',
            field=models.TimeField(default='00:00:00', verbose_name='Depature time'),
        ),
    ]
