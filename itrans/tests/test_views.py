from django.test import TestCase

from itrans.models import Company
from django.core.urlresolvers import reverse


class CompanyListViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_companies = 5
        for company_num in range(number_of_companies):
            Company.objects.create(
                name='Company_%s' % company_num,
                prename = 'Prename %s' % company_num,)

    def test_url_exist_at_desired_location(self):
        resp = self.client.get('/company_list/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('company_list'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('company_list'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/companies.html')

    def test_lists_all_companies(self):
        # Get all 5 companies in the list
        resp = self.client.get(reverse('company_list'))
        self.assertEqual(resp.status_code, 200)
        self.assertTrue( len(resp.context['company_list']) == 5)


class CompanyDetailViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        number_of_companies = 5
        for company_num in range(number_of_companies):
            Company.objects.create(
                name='Company_%s' % company_num,
                prename = 'Prename %s' % company_num,)

    def test_url_exist_at_desired_location(self):
        resp = self.client.get('/company_details/1/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('company_details', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('company_details', kwargs={'pk': 1}))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/company_details.html')


class TerminalTerrestreDetailViewTest(TestCase):

    def test_url_exist_at_desired_location(self):
        resp = self.client.get('/terminal_terrestre/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('terminal_terrestre'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('terminal_terrestre'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/terminal_terrestre.html')


class IndexViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        pass

    def test_url_exist_at_desired_location(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('index'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/index.html')


class SheduleListViewTest(TestCase):

    def setUp(self):
        pass

    def test_url_exist_at_desired_location(self):
        resp = self.client.get('/schedule/')
        self.assertEqual(resp.status_code, 200)

    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('schedule'))
        self.assertEqual(resp.status_code, 200)

    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('schedule'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'core/schedule.html')

    def test_view_url_with_kwargs_accessible_by_name(self):
        resp = self.client.get(reverse('schedule', kwargs={'direction': 'from'}))
        self.assertEqual(resp.status_code, 200)







