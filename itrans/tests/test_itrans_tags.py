from django.test import TestCase
from itrans.models import Company
from itrans.templatetags.itrans_tags import get_all_company_list_ordered_by_name

class Get_all_company_list_ordered_by_name_Function_Test(TestCase):

    @classmethod
    def setUpTestData(cls):
        """ Set up non-modified objects used by all test methods """
        # setUpTestData: Run once to set up non-modified data for all class methods.
        Company.objects.create(name='Company5', prename='Pre_company_5')
        Company.objects.create(name='Company3', prename='Pre_company__3')
        Company.objects.create(name='Company2', prename='Pre_company2')
        Company.objects.create(name='Company1', prename='Pre_company___1')
        Company.objects.create(name='Company4', prename='Pre_company_____4')

    def test_company_list_ordered_by_name(self):
        expected_ordered_company_list = ['Company1', 'Company2', 'Company3', 'Company4', 'Company5',]
        tested_list = [(company.name) for company in get_all_company_list_ordered_by_name()]
        self.assertEqual(tested_list, expected_ordered_company_list)

