import json
from rest_framework import status
from django.test import TestCase, Client
from django.urls import reverse
from ..models import Company
from ..api.serializers import CompanySerializer

# initialize the APIClient app
client = Client()

class GetAllCompaniesTest(TestCase):
    """ Test module for GET all puppies API """

    def setUp(self):
        Company.objects.create(
            name='Company_1', prename='Precompany_1')
        Company.objects.create(
            name='Company_2', prename='Precompany_2')
        Company.objects.create(
            name='Company_3', prename='Precompany_3')
        Company.objects.create(
            name='Company_4', prename='Precompany_4')

    def test_get_all_companies(self):
        # get API response
        response = client.get(reverse('get_post_companies'))
        # get data from db
        companies = Company.objects.all()
        serializer = CompanySerializer(companies, many=True)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetSingleCompanyTest(TestCase):
    """ Test module for GET single company API """

    def setUp(self):
        self.office = Company.objects.create(
            name='Office', prename='Precompany_1')
        self.country = Company.objects.create(
            name='Country', prename='Precompany_2')
        self.newstyle = Company.objects.create(
            name='Newstyle', prename='Precompany_3')
        self.softy = Company.objects.create(
            name='Softy', prename='Precompany_4')

    def test_get_valid_single_company(self):
        response = client.get(
            reverse('get_delete_update_company', kwargs={'pk': self.newstyle.pk}))
        company = Company.objects.get(pk=self.newstyle.pk)
        serializer = CompanySerializer(company)
        self.assertEqual(response.data, serializer.data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_get_invalid_single_company(self):
        response = client.get(
            reverse('get_delete_update_company', kwargs={'pk': 30}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

