from django.test import TestCase
from itrans.models import Company, Route, Trip, Stop


class CompanyModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        """ Set up non-modified objects used by all test methods """
        # setUpTestData: Run once to set up non-modified data for all class methods.
        Company.objects.create(name='Azuay', prename='Cooperativa')


    def test_object_name_is_name(self):
        company = Company.objects.get(id=1)
        expected_object_name = '%s' % (company.name)
        self.assertEqual(str(company), expected_object_name)

    def test_get_absolute_url(self):
        """ This will also fail if the urlconf is not defined. """
        company = Company.objects.get(id=1)
        self.assertEqual(company.get_absolute_url(), '/company_details/1/')


class RouteModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        """ Set up non-modified objects used by all test methods """
        Company.objects.create(name='Mycompany', prename='Cooperative')
        company = Company.objects.get(id=1)
        Route.objects.create(company=company,
                             from_end='Begin of route',
                             stopover='Middle of route',
                             stopover2='Middle second',
                             to_end='End of route')
        Route.objects.create(company=company,
                             from_end='Begin of route',
                             to_end='End of route')

    def test_object_name_is_company_name_plus_route_name(self):
        route = Route.objects.get(id=1)
        expected_object_name = '%s: %s' % (route.company.name, route.route_name)
        self.assertEqual(str(route), expected_object_name)

    def test_object_name_is_company_name_plus_route_name_with_some_blank_fields(self):
        route = Route.objects.get(id=2)
        expected_object_name = '%s: %s' % (route.company.name, route.route_name)
        self.assertEqual(str(route), expected_object_name)


class TripModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Company.objects.create(name='Mycompany', prename='Cooperative')
        company = Company.objects.get(id=1)
        Route.objects.create(company=company,
                             from_end='Begin of route',
                             stopover='Middle of route',
                             stopover2='Middle second',
                             to_end='End of route')
        route = Route.objects.get(id=1)
        Trip.objects.create(route=route,
                            start_time='06:33:17')

    def test_object_name_is_route_name_plus_start_time(self):
        route = Route.objects.get(id=1)
        trip = Trip.objects.get(id=1)
        expected_object_name = '%s: %s' % (str(route), str(trip.start_time))
        self.assertEquals(str(trip), expected_object_name)

    def test_code_from_service(self):
        route = Route.objects.get(id=1)
        trip = Trip.objects.get(id=1)
        expected_code_from_service = '%s: %s: %s' % (route.company.name, route.route_name, str(trip.start_time))
        self.assertEquals(trip.code_from_service, expected_code_from_service)


class StopModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Company.objects.create(name='Mycompany', prename='Cooperative')
        company = Company.objects.get(id=1)
        Route.objects.create(company=company,
                             from_end='Begin of route',
                             stopover='Middle of route',
                             stopover2='Middle second',
                             to_end='End of route')
        route = Route.objects.get(id=1)
        Stop.objects.create(route=route,
                            name='Bus stop')

    def test_object_name_is_bus_name(self):
        stop = Stop.objects.get(id=1)
        expected_object_name = '%s' % (stop.name)
        self.assertEquals(str(stop), expected_object_name)




