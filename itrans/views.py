# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from functools import reduce
import operator

from django.shortcuts import render, redirect
from django.db.models import Q
from django.views.decorators.csrf import csrf_exempt
from django.views import generic

from .models import Company, Route, Trip
from .forms import FilterForm, BackMessageForm, FORM_COMPANIES



all_companies = sorted([comp[0] for comp in FORM_COMPANIES])

# filer order options
order_to_list = ['route__route_finish__name', 'start_time', 'route__company__name']
order_from_list = ['route__route_start__name', 'start_time', 'route__company__name']
order_by_company_list = ['route__company','route__route_start__name',
                         'route__route_finish__name', 'start_time']

def index(request):
    """ Prepare the initial page of the itrans application """
    companies = Company.objects.all().order_by('name')
    routes = Route.objects.all().order_by('route_name', 'company__name')
    trips = Trip.objects.all().order_by(*order_by_company_list)

    return render(request, 'core/index.html', {'companies': companies,
                                               'routes': routes,
                                               'trips': trips})


def contacts(request):
    """ Contacts for connection with service manager;
        form for feedback message from current user """
    if request.method == 'POST':
        user = request.user
        form = BackMessageForm(request.POST)
        if form.is_valid():
            print('Form is valid')
            message = form.save(commit=False)
            message.user = user
            message.save()
            return redirect('/')

    else:
        form = BackMessageForm()
    return render(request, 'core/contacts.html', {'form': form})



def schedule(request, direction='from'):
    """ Schedule page with filter form preparation """
    filter_list = []
    template = 'core/schedule.html'

    if request.method == 'POST':
        form = FilterForm(request.POST)
        res_set = request.POST.copy()
        show_from = res_set['show_from']
        show_to = res_set['show_to']
        companies = res_set.getlist('company_choices')
        company_list = [Q(route__company__name=company)
                                    for company in companies]

        if show_from != 'any':
            filter_list.append(Q(route__route_start__name=show_from))

        if show_to != 'any':
            filter_list.append(Q(route__route_finish__name=show_to))

        if (show_to == 'any') & (show_from == 'any'):
            trips = Trip.objects \
                .filter(reduce(operator.or_, company_list)) \
                .order_by(*order_from_list)
        else:
            trips = Trip.objects \
                .filter(reduce(operator.and_, filter_list)) \
                .filter(reduce(operator.or_, company_list)) \
                .order_by(*order_from_list)

        return render(request, template, {
            'trips': trips,
            'company_list': company_list,
            'form': form })

    if direction == 'from':
        trips = Trip.objects \
            .filter(route__route_start__name='Cuenca') \
            .order_by(*order_to_list)

        form = FilterForm(initial={'show_from': 'Cuenca',
                                   'show_to': 'any',
                                   'company_choices': all_companies})
    else:
        trips = Trip.objects \
            .filter(route__route_finish_name='Cuenca') \
            .order_by(*order_from_list)

        form = FilterForm(initial={'show_from': 'any',
                                   'show_to': 'Cuenca',
                               'company_choices': all_companies})

    return render(request, template,
                  {'trips': trips, 'form': form})

@csrf_exempt
def ajax_schedule_data(request):
    """ Transfer data with AJAX to the schedule table """
    if request.is_ajax():
        if request.method == 'POST':
            filter_list = []
            template = 'core/ajax_schedule_table.html'
            res_set = request.POST.copy()
            show_from = res_set['show_from']
            show_to = res_set['show_to']
            checkedCompanies = res_set['checkedCompanies'].split(',')
            company_list = [Q(route__company__name=company)
                                            for company in checkedCompanies]

            if show_from != 'any':
                filter_list.append(Q(route__route_start__name=show_from))

            if show_to != 'any':
                filter_list.append(Q(route__route_finish__name=show_to))

            if (show_to == 'any') & (show_from == 'any'):
                trips = Trip.objects \
                    .filter(reduce(operator.or_, company_list)) \
                    .order_by(*order_from_list)
            else:
                trips = Trip.objects \
                    .filter(reduce(operator.and_, filter_list)) \
                    .filter(reduce(operator.or_, company_list)) \
                    .order_by(*order_from_list)

            return render(request, template,
                          {'trips': trips})


class CompanyListView(generic.ListView):
    """ Generic view to fetch the company list and then show in a list """
    context_object_name = 'companies'
    template_name = 'core/companies.html'

    def get_queryset(self):
        return Company.objects.all().order_by('name')


class CompanyDetailsView(generic.DetailView):
    """ Generic view to show the details of a particular company  """
    model = Company
    template_name = 'core/company_details.html'


def company_details(request, pk):
    """ Prepare data to show the details page of the company """
    company = Company.objects.get(pk=pk)
    routes = Route.objects.filter(company=company).order_by(
                                        'to_end', 'stopover', 'stopover2')
    trips = Trip.objects.filter(route__company__name=company).order_by(
                                        *order_by_company_list)

    return render(request, 'core/company_details.html',
                          {'company':company,
                           'routes': routes,
                           'trips': trips})


def terminal_terrestre(request):
    """ Prepare data to show the Terminal Terrestre page """
    return render(request, 'core/terminal_terrestre.html')

