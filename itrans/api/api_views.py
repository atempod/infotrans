from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from ..models import Company
from .serializers import CompanySerializer


@api_view(['GET', 'DELETE', 'PUT'])
def get_delete_update_company(request, pk):
    try:
        company = Company.objects.get(pk=pk)
    except Company.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    # get details of a single company
    if request.method == 'GET':
        serializer = CompanySerializer(company)
        return Response(serializer.data)

    # delete a single company
    elif request.method == 'DELETE':
        return Response({})
    # update details of a single company
    elif request.method == 'PUT':
        return Response({})




@api_view(['GET', 'POST'])
def get_post_companies(request):
    # get all companies
    if request.method == 'GET':
        companies = Company.objects.all()
        serializer = CompanySerializer(companies, many=True)
        return Response(serializer.data)
    # insert a new record for a company
    elif request.method == 'POST':
        return Response({})





