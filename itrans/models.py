# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from datetime import datetime, timedelta, date, time

from django.db.models import Q
from django.db import models
from django.urls import reverse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


def default_start_datetime():
    """ Prepare start_time (for example, middlenight) value """
    now = datetime.now()
    start = now.replace(hour=0, minute=0, second=0, microsecond=0)
    return start if start > now else start + timedelta(days=1)

def default_start_time():
    """ Prepare start_time (for example, middlenight) value """
    now = datetime.now()
    return now.replace(hour=0, minute=0, second=0, microsecond=0).time()

def time2minutes_diff(time1, minutes):
    departure_datetime = datetime.combine(date(2, 2, 2), time1)
    return (departure_datetime - timedelta(minutes=minutes)).time()

def time2duration_diff(time1, duration):
    time_datetime = datetime.combine(date(2, 2, 2), time1)
    return (time_datetime - duration).time()

def time2time_diff(time1, time2):
    return datetime.combine(date(1,1,1), time1) - \
           datetime.combine(date(1,1,1), time2)


class CustomTimeField(models.TimeField):
    pass


class Province(models.Model):
    """ Province model """
    name = models.CharField('Name', max_length=100, unique=True)
    code = models.CharField('Code', max_length=5, null=True, blank=True)
    code_group = models.CharField('Code Group', max_length=20, null=True, blank=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name = 'Province'
        verbose_name_plural = 'Provinces'


class Canton(models.Model):
    """ Canton model """
    name = models.CharField('Name', max_length=100, unique=True)
    province = models.ForeignKey(Province, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Canton'
        verbose_name_plural = 'Cantons'


class City(models.Model):
    """ City model """
    STATUSES = (
        ('unknown', 'Unknown'),
        ('capital', 'Capital'),
        ('city', 'City'),
        ('village', 'Village'),
    )

    name = models.CharField('Name', max_length=100, unique=True)
    code = models.IntegerField('Code', blank=True, null=True)
    canton = models.ForeignKey(Canton, on_delete=models.CASCADE, blank=True, null=True)
    province = models.ForeignKey(Province, on_delete=models.CASCADE, blank=True, null=True)
    status = models.CharField(verbose_name=u'Status', max_length=20,
                              choices=STATUSES, default=STATUSES[0][0])
    # point = models.PointField(verbose_name=u'Point', blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'City'
        verbose_name_plural = 'Cities'

    def city_code_compose(self):
        """ City code generator is proposed to be here """
        code = 00
        return code

    def save(self, *args, **kwargs):
        # self.code = self.city_code_compose()
        super(City, self).save(*args, **kwargs)


class Company(models.Model):
    """ Company model"""
    get_file_path='images'

    name = models.CharField('Name', max_length=50, unique=True)
    full_name = models.CharField('Full Name', max_length=100, blank=True, null=True)
    code = models.CharField('Code', max_length=10, default='XX', blank=True, null=True)
    prename = models.CharField('Prename', max_length=100, blank=True, null=True)
    is_terminal_terrestre = models.BooleanField('Terminal Terrestre', default=True)
    terminal = models.CharField('Terminal', max_length=100, blank=True, null=True)
    terminal_address = models.CharField('Terminal address', max_length=500, blank=True, null=True)
    office_address = models.CharField('Office address', max_length=500, blank=True, null=True)
    office_opening = models.TimeField('Office opening time', blank=True, null=True)
    office_closing = models.TimeField('Office opening time', blank=True, null=True)
    terminal_map = models.CharField('Terminal map', max_length=500, blank=True, null=True)
    office_map = models.CharField('Office map', max_length=500, blank=True, null=True)
    website = models.CharField('Web site', max_length=250, blank=True, null=True)
    website2 = models.CharField('Web site-2', max_length=250, blank=True, null=True)
    email = models.CharField('Email', max_length=500, blank=True, null=True)
    phone = models.CharField('Phone number', max_length=25, blank=True, null=True)
    phone2 = models.CharField('Phone number-2', max_length=25, blank=True, null=True)
    phone_tickets = models.CharField('Phone for tickets', max_length=25, blank=True, null=True)
    phone_help = models.CharField('Phone for help', max_length=25, blank=True, null=True)
    rating = models.IntegerField('Rating', blank=True, null=True)
    comments = models.CharField('Comments', max_length=1000, blank=True, null=True)
    use_minibus = models.BooleanField('Use minibus', default=False)
    description = models.CharField('Description', max_length=1000, blank=True, null=True)
    logo = models.ImageField(verbose_name=u'Logo', upload_to=get_file_path,
                                    max_length=256, blank=True, null=True)
    img_bus = models.ImageField(verbose_name=u'Bus img', upload_to=get_file_path,
                                    max_length=256, blank=True, null=True)
    img_bus2 = models.ImageField(verbose_name=u'Bus2 img', upload_to=get_file_path,
                                    max_length=256, blank=True, null=True)
    img_terminal = models.ImageField(verbose_name=u'Terminal', upload_to=get_file_path,
                                    max_length=256, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    class Meta:
        verbose_name = 'Company'
        verbose_name_plural = 'Companies'

    def __str__(self):
        return  self.name

    def save(self, *args, **kwargs):
        self.code = self.name[:3].upper()
        super(Company, self).save(*args, **kwargs)

    def get_absolute_url(self):
        # Absolute url for company details version 2
        return reverse('company_details', args=[str(self.id)])

    def get_absolute_url_v1(self):
        # Absolute url for company details version 1
        return reverse('company_details_v1', args=[str(self.id)])


class Station(models.Model):
    """ Station model. ForeignKey to City. """
    STATUSES = (
        ('unknown', 'Unknown'),
        ('city', 'In City'),
        ('village', 'In Village'),
        ('by_road', 'By the Road'),
    )

    name = models.CharField('Name', max_length=100, blank=True, null=True)
    city = models.ForeignKey(City, on_delete=models.CASCADE, blank=True, null=True)
    # canton = models.ForeignKey(Canton, on_delete=models.CASCADE, blank=True, null=True)
    # province = models.ForeignKey(Province, on_delete=models.CASCADE, blank=True, null=True)
    route = models.ManyToManyField('Route', through='RouteStation', related_name='stations')
    full_name = models.CharField('FullName', max_length=100, blank=True, null=True)
    status = models.CharField(verbose_name=u'Status', max_length=20,
                                    choices=STATUSES, default=STATUSES[0][0])
    is_terminal = models.BooleanField('is a Terminal', default=False)
    description = models.CharField('Description', max_length=500, blank=True, null=True)
    # point = models.PointField(verbose_name=u'Point', blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name = 'Station'
        verbose_name_plural = 'Stations'

    def name_compose(self):
        """ Compose name of the station """
        if self.name:
            name = self.name
        else:
            name = self.city.name if self.city else 'Noname stop'
        return name

    def full_name_compose(self):
        """ Compose full name of the station """
        return self.province.name + ": " + self.name if self.province else None

    def save(self, *args, **kwargs):
        """ Override station name
        """
        self.name = self.name_compose()
        self.full_name = self.full_name_compose()
        super(Station, self).save(*args, **kwargs)


class SourceTrip(models.Model):
    """ Source of current data for trip """
    name = models.CharField('Name', max_length=100, unique=True)
    notes = models.CharField('Notes', max_length=500, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return  self.name

    class Meta:
        verbose_name = 'Source'
        verbose_name_plural = 'Sources'


class Route(models.Model):
    """ Route model. ForeignKey to Company. """
    company = models.ForeignKey(Company, on_delete=models.CASCADE, default=3)
    route_name = models.CharField('Name', max_length=100, blank=True, null=True)
    route_full_name = models.CharField('Full name', max_length=100,
                            blank=True, null=True, default='Not defined yet')
    route_brand_name = models.CharField('Brand name', max_length=100, blank=True, null=True)
    route_suffix = models.CharField('Suffix', max_length=100, blank=True, null=True)
    code = models.IntegerField('Code', blank=True, null=True, default=1)
    code_full = models.CharField('Full code', max_length=20, blank=True, null=True)

    from_end = models.CharField('Departure city', max_length=100, blank=True, null=True)
    to_end = models.CharField('Arrival city', max_length=100, blank=True, null=True)
    stopover = models.CharField('Stopover', max_length=100, blank=True, null=True)
    stopover2 = models.CharField('Stopover 2', max_length=100, blank=True, null=True)

    route_pre_start = models.ForeignKey(Station, on_delete=models.CASCADE,
                                related_name='route_pre_start', blank=True, null=True)
    route_start = models.ForeignKey(Station, on_delete=models.CASCADE,
                                related_name='route_start', blank=True, null=True)
    route_finish = models.ForeignKey(Station, on_delete=models.CASCADE,
                                related_name='route_finish', blank=True, null=True)
    route_post_finish = models.ForeignKey(Station, on_delete=models.CASCADE,
                                related_name='route_post_start', blank=True, null=True)
    via_city = models.ForeignKey(Station, on_delete=models.CASCADE,
                                related_name='via_city', blank=True, null=True)
    via_city2 = models.ForeignKey(Station, on_delete=models.CASCADE,
                                 related_name='via_city2', blank=True, null=True)

    is_express = models.BooleanField('Express', default=False)
    is_archived = models.BooleanField('Archived', default=False)
    # is_return = models.BooleanField('Return trip', default=False)

    price = models.DecimalField('Price', max_digits=5, decimal_places=2, blank=True, null=True)
    duration = models.CharField('Duration', max_length=10, blank=True, null=True)
    full_time = models.DurationField('Full time', max_length=10, default=timedelta(minutes=0), blank=True, null=True)
    distance = models.IntegerField('Full distance', blank=True, null=True)
    bus_type = models.CharField('Bus type', max_length=100, blank=True, null=True)

    source = models.ForeignKey(SourceTrip, on_delete=models.CASCADE, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)
    description = models.CharField('Description', max_length=1000, blank=True, null=True)

    def __str__(self):
        return "%s: %s" % (self.company.name, self.route_full_name)

    class Meta:
        verbose_name = 'Route'
        verbose_name_plural = 'Routes'

    def route_name_compose(self):
        """ Compose route name from several route parameters """
        route_name = None
        if self.route_start and self.route_finish:
            route_name = '%s - %s' % (self.route_start, self.route_finish)
        return route_name

    def route_full_name_compose(self):
        """ Compose route name from several route parameters """
        route_name = None
        if self.route_start and self.route_finish:
            if self.route_pre_start:
                route_name = '%s - %s' % (self.route_pre_start, self.route_start)
            else:
                route_name = '%s' % self.route_start
            if self.via_city:
                route_name = '%s - %s' % (route_name, self.via_city)
            if self.via_city2:
                route_name = '%s - %s' % (route_name, self.via_city2)
            route_name = '%s - %s' % (route_name, self.route_finish)
            if self.route_post_finish:
                route_name = '%s - %s' % (route_name, self.route_post_finish)
        return route_name

    def code_compose(self, code_prefix):
        """ Compose route code_full from several route parameters """
        code = 1
        routes = Route.objects.filter(code_full__startswith=code_prefix)\
                              .order_by('code')
        last_code = routes.last().code if routes else 0
        code += last_code
        return code


    def code_prefix_compose(self):
        """ Compose route code_prefix before code and code_full gathering """
        if self.route_start.city.name == 'Cuenca':
            direction = '1'                     # from Cuenca
            city_code = self.route_finish.city.code
        else:
            direction = '2'                     # to Cuenca
            city_code = self.route_start.city.code
        if city_code <= 9:
            city_code_str = "00" + str(city_code)
        elif city_code <= 99:
            city_code_str = "0" + str(city_code)
        else:
            city_code_str = str(city_code)
        return self.company.code + '-' + direction + city_code_str

    def duration_compose(self):
        """ Auto fill the Route Duration fiels as string from full fime """
        duration = str(self.full_time)[:-3] if self.full_time else None
        return duration

    def save(self, *args, **kwargs):
        """ Override route_name on the basis of several route attributes
            Override save model with the route_name value
        """
        full_code_prefix = self.code_prefix_compose()
        via_city = self.via_city
        via_city2 = self.via_city2
        if self.id:
            print('the route exist')
            code = self.code
        else:
            print('the route is absent')
            self.via_city = via_city
            self.via_city2 = via_city2
            code = self.code_compose(full_code_prefix)

        self.code = code
        self.code_full = full_code_prefix + "-" + str(code)
        self.duration = self.duration_compose()

        self.route_name = self.route_name_compose()
        self.route_full_name = self.route_full_name_compose()

        super(Route, self).save(*args, **kwargs)


class RouteStation(models.Model):
    """ Through table between Route and Station. M2M relation. """
    STOPTYPES = (
        ('stopover', 'Stopover'),
        ('pre_start', 'PreStart'),
        ('start', 'Start'),
        ('finish', 'Finish'),
        ('post_finish', 'PostFinish'),
        ('via_city', 'viaCity'),
        ('via_city2', 'viaCity2'),
    )
    DEFAULT_STOP_SERIAL = 2
    route = models.ForeignKey(Route, related_name='routes',
                                                    on_delete=models.CASCADE)
    code_full = models.CharField('Full code', max_length=20, blank=True, null=True)
    station = models.ForeignKey(Station, related_name='stations',
                                                    on_delete=models.CASCADE)
    route_name = models.CharField('Route name', max_length=100,
                                                    blank=True, null=True)
    stoptype = models.CharField(verbose_name=u'Stop Type', max_length=20,
                              choices=STOPTYPES, default=STOPTYPES[0][0])
    serial_in_route = models.IntegerField('# in route',
                                                    default=DEFAULT_STOP_SERIAL)

    arrival = models.TimeField('Arrival', blank=True, null=True)
    departure = models.TimeField('Departure', blank=True, null=True)
    duration = models.IntegerField('Duration', blank=True, null=True, default=0)
    time_from_start = models.IntegerField('Time from start', blank=True, null=True)
    dist_from_start = models.IntegerField('Distance from start', blank=True, null=True)
    notes = models.CharField('Notes', max_length=500, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return "%s: %s" % (self.route.route_name, self.station.name)

    class Meta:
        verbose_name = 'Route Station'
        verbose_name_plural = 'Route Stations'

    def save(self, *args, **kwargs):
        self.code_full = Route.objects.get(id=self.route.id).code_full
        route_name = self.route_name
        station = self.station
        self.duration = self.duration if self.duration else 0
        if self.id:
            print('the route exist')
            self.arrival = time2minutes_diff(self.departure, self.duration)
        else:
            print('the route is absent')
        if self.stoptype == 'start':
            self.station = station
            self.route_name = route_name

        print('>>>>>>>> RouteStation creation <<<<<<<<<<<')
        super(RouteStation, self).save(*args, **kwargs)


class Trip(models.Model): #  StopTiming from Trip
    """ Trip model. ForeignKey to Route. """
    route = models.ForeignKey('Route', on_delete=models.CASCADE)
    code = models.IntegerField('Code', blank=True, null=True, default=1)
    code_from_service = models.CharField('Service code', max_length=100, blank=True, null=True)
    code_from_company = models.CharField('Company code', max_length=100, blank=True, null=True)
    name_full = models.CharField('Full name', max_length=100, blank=True, null=True)
    name_brand = models.CharField('Brand name', max_length=100, blank=True, null=True)
    start_time = models.TimeField('Departure time', default='00:00:00')
    days = models.CharField('Week days', max_length=100, blank=True, null=True)
    is_archived = models.BooleanField('Archived', default=False)
    is_draft = models.BooleanField('Draft', default=True)
    notes = models.CharField('Notes', max_length=200, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        # The best way to name by 'code_from_service = Company + Route + start_time'
        return "%s: %s" % (self.route, self.start_time)

    class Meta:
        unique_together = ('route', 'start_time',)
        verbose_name = 'Trip'
        verbose_name_plural = 'Trips'

    def code_from_service_compose(self):
        """ Compose code_from_service parameter from 'company+route+start_time' """
        return '%s: %s' % (self.route.code_full,  self.start_time)

    def name_full_compose(self):
        """ Compose code_from_service parameter from 'company+route+start_time' """
        return '%s: %s: %s' % (self.route.company.name, self.route.route_name,
                                                        str(self.start_time))

    def save(self, *args, **kwargs):
        """ Override code_from_service parameter and save model
        """
        self.name_full = self.name_full_compose()
        self.code_from_service = self.code_from_service_compose()
        super(Trip, self).save(*args, **kwargs)


class Stop(models.Model):
    """ Trip stop_list model. ForeignKey to Route. """
    STOPTYPES = (
        ('stopover', 'Stopover'),
        ('pre_start', 'PreStart'),
        ('start', 'Start'),
        ('finish', 'Finish'),
        ('post_finish', 'PostFinish'),
        ('via_city', 'viaCity'),
        ('via_city2', 'viaCity2'),
    )

    DEFAULT_TRIP_ID = 1
    DEFAULT_STOP_SERIAL = 1

    trip = models.ForeignKey('Trip', on_delete=models.CASCADE, default=DEFAULT_TRIP_ID)
    station = models.ForeignKey('Station', on_delete=models.CASCADE)
    name = models.CharField('Stop name', max_length=100, blank=True, null=True)
    stoptype = models.CharField(verbose_name=u'Stop Type', max_length=20,
                              choices=STOPTYPES, default=STOPTYPES[0][0])
    serial_in_route = models.IntegerField('# of stop', default=DEFAULT_STOP_SERIAL)
    is_stopping = models.BooleanField(default=True)
    arrival = models.TimeField('Arrival time', blank=True, null=True)
    departure = models.TimeField('Departure time', blank=True, null=True)
    duration = models.IntegerField('Stop duration', blank=True, null=True)
    time_from_start = models.IntegerField('Time from start', blank=True, null=True)
    dist_from_start = models.IntegerField('Distance from start', blank=True, null=True)
    notes = models.CharField('Notes', max_length=500, blank=True, null=True)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                        auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                        auto_now_add=False, auto_now=True)

    def __str__(self):
        return self.station.name

    class Meta:
        verbose_name = 'Stop'
        verbose_name_plural = 'Stops'


class BackMessage(models.Model):
    """ Feedback message model. ForeignKey to User. """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField('Title', max_length=100)
    text = models.TextField('Message', max_length=1000)
    created_at = models.DateTimeField('Creation date', auto_now_add=True,
                                    auto_now=False, null=True, blank=True)
    updated_at = models.DateTimeField('Update date',
                                    auto_now_add = False, auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Message'
        verbose_name_plural = 'Messages'


@receiver(post_save, sender=Route)
def create_route_stations(sender, instance, created, **kwargs):
    """ Create preliminary set of RouteStation instances
        by signal of created Route instance
        (based on the Route instance)
    """
    print('to be created ===========================================', instance)
    key_stations = {'start': instance.route_start,
                    'finish': instance.route_finish,
                    'via_city': instance.via_city,
                    'via_city2': instance.via_city2}

    serials = {'start': 1,
                'finish': 10000,
                'stopover': 2,
                'via_city': 3,
                'via_city2': 4
                }

    for el in key_stations.keys():
        print(el)
        if created:     # Create RouteStation right after the Route creation
            if key_stations[el]:
                if el == 'start':
                    arrival = None
                    departure = default_start_time()
                elif el == 'finish':
                    arrival = (default_start_datetime() +
                               instance.full_time).time()
                    departure = None
                else:
                    departure = default_start_time()
                    arrival = default_start_time()
                RouteStation.objects.create(route=instance,
                                            station=key_stations[el],
                                            stoptype=el,
                                            serial_in_route=serials[el],
                                            arrival=arrival,
                                            departure=departure,
                                            )
            print('created ===============================================')

        else:           # Update RouteStation right after the Route renewal
            RouteStation.objects.filter(Q(route=instance) & Q(stoptype=el)).\
                                 update(station=key_stations[el],
                                        code_full = instance.code_full)
            print('updated ===============================================')


# @receiver(post_save, sender=RouteStation)
def update_key_stations(sender, instance, created, **kwargs):
    """ Update fields of Route instance
        by signal from saved (not created!) RouteStation instance
        (based on the set of RouteStation instances)
    """
    print('wwwwwwwwwwwwwwwwwwwwwwwwwwwww __________ save == created or updated')
    if not created:
        print(
            'wwwwwwwwwwwwwwwwwwwwwwwwwwwwww __________ save == only if updated')
        route_stations = RouteStation.objects.filter(route=instance.route)
        print('route_stations ===', route_stations)

        route = Route.objects.filter(id=instance.route.id).first()
        print('route', route, route.route_start)

        stoptype = instance.stoptype
        if instance.stoptype == 'start' or instance.stoptype == 'finish':
            stoptype = 'route_' + instance.stoptype
        stoptype_value = instance.station

        update_kwargs = {
            "{}".format(stoptype): stoptype_value
        }
        update_kwargs['route_name'] = 'NEW_NEW NAME'

        # Update Route key stations
        Route.objects.filter(id=instance.route.id).update(**update_kwargs)
        # Trigger to update Route.route_name
        route_to_update = Route.objects.get(id=instance.route.id)
        route_to_update.stoptype = stoptype_value
        route_to_update.route_name = 'RouteName change Trigger'
        route_to_update.save()


@receiver(post_save, sender=Trip)
def create_stops(sender, instance, created, **kwargs):
    """ Create set of Stop instances with predetermined data
        by signal of Trip instance creation
        (based on the set of RouteStation instances)
    """
    if created:
        route_stations = RouteStation.objects.filter(route=instance.route)
        for rs in route_stations:
            time_shift = time2time_diff(instance.start_time,
                                            default_start_time())
            departure = time2duration_diff(rs.departure, -time_shift) \
                                            if rs.stoptype != 'finish' else None
            arrival = time2duration_diff(rs.arrival, -time_shift) \
                                            if rs.stoptype != 'start' else None

            Stop.objects.create(trip=instance, station=rs.station,
                                stoptype=rs.stoptype, departure=departure,
                                serial_in_route=rs.serial_in_route,
                                arrival=arrival, duration=rs.duration,
                                )
