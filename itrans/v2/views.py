# -*- coding: utf-8 -*-
from functools import reduce
import operator

from django.shortcuts import render, redirect
from django.db.models import Q
from django.views import generic
from django.views.decorators.csrf import csrf_exempt

from ..models import Company, Route, Trip
from ..forms import FilterForm, BackMessageForm, FORM_COMPANIES

all_companies = sorted([comp[0] for comp in FORM_COMPANIES])

# filer order options
order_to_list = ['route__route_finish__name', 'start_time', 'route__company__name']
order_from_list = ['route__route_start__name', 'start_time', 'route__company__name']
order_by_company_list = ['route__company','route__route_start__name',
                         'route__route_finish__name', 'start_time']


class CompanyListView(generic.ListView):
    """ Generic view to fetch the company list and then show in a list """
    context_object_name = 'companies'
    template_name = 'v2/companies.html'

    def get_queryset(self):
        return Company.objects.all().order_by('name')


class AboutProjectView(generic.ListView):
    """ Generic view to fetch the company list and then show in a list """
    context_object_name = 'companies'
    template_name = 'v2/about_project.html'

    def get_queryset(self):
        return Company.objects.all().order_by('name')


def feedback(request):
    """ Contact form page for messaging from current user """
    if request.method == 'POST':
        user = request.user
        form = BackMessageForm(request.POST)
        if form.is_valid():
            print('Form is valid')
            message = form.save(commit=False)
            message.user = user
            message.save()
            return redirect('/')
    else:
        form = BackMessageForm()

    return render(request, 'v2/feedback.html', {'form': form})


def routes_map(request):
    """ Prepare the initial page of the itrans application """
    companies = Company.objects.all().order_by('name')
    company_list = Company.objects.all().order_by('name')
    routes = Route.objects.all().order_by('route_name').distinct('route_name')
    trips = Trip.objects.all().order_by(*order_by_company_list)

    return render(request, 'v2/routes_map.html', {'companies': companies,
                                                'company_list': company_list,
                                                'routes': routes,
                                                'trips': trips})


class RoutesMapView(generic.ListView):
    """ Generic view to fetch the company list and then show in a list """
    context_object_name = 'routes_map'
    template_name = 'v2/routes_map.html'

    def get_queryset(self):
        return Route.objects.all()


class TerminalTerrestreView(generic.ListView):
    """ Generic view to fetch the company list and then show in a list """
    context_object_name = 'terminal_terrestre'
    template_name = 'v2/terminal_terrestre.html'

    def get_queryset(self):
        return Company.objects.all().order_by('name')


def company_details(request, pk):
    """ Prepare data to show the details page of the company """
    company = Company.objects.get(pk=pk)
    routes = Route.objects.filter(company=company).order_by(
                                        'to_end', 'stopover', 'stopover2')
    trips = Trip.objects.filter(route__company__name=company).order_by(
                                        *order_by_company_list)

    return render(request, 'v2/company_details.html',
                          {'company':company,
                           'routes': routes,
                           'trips': trips})


def index(request):
    """ Prepare the initial page of the itrans application """
    companies = Company.objects.all().order_by('name')
    company_list = Company.objects.all().order_by('name')
    routes = Route.objects.all().order_by('route_name', 'company__name')
    trips = Trip.objects.all().order_by(*order_by_company_list)

    return render(request, 'v2/index.html', {'companies': companies,
                                             'company_list': company_list,
                                               'routes': routes,
                                               'trips': trips})


def schedule(request, direction='from'):
    """ Schedule page with filter form preparation """
    filter_list = []
    template = 'v2/schedule.html'

    if request.method == 'POST':
        form = FilterForm(request.POST)
        res_set = request.POST.copy()
        show_from = res_set['show_from']
        show_to = res_set['show_to']
        companies = res_set.getlist('company_choices')
        company_list = [Q(route__company__name=company)
                                    for company in companies]

        if show_from != 'any':
            filter_list.append(Q(route__route_start__name=show_from))

        if show_to != 'any':
            filter_list.append(Q(route__route_finish__name=show_to))

        if (show_to == 'any') & (show_from == 'any'):
            trips = Trip.objects \
                .filter(reduce(operator.or_, company_list)) \
                .order_by(*order_from_list)
        else:
            trips = Trip.objects \
                .filter(reduce(operator.and_, filter_list)) \
                .filter(reduce(operator.or_, company_list)) \
                .order_by(*order_from_list)

        return render(request, template, {
            'trips': trips,
            'company_list': company_list,
            'form': form })

    if direction == 'from':
        trips = Trip.objects \
            .filter(route__route_start__name='Cuenca') \
            .order_by(*order_to_list)

        form = FilterForm(initial={'show_from': 'Cuenca',
                                   'show_to': 'any',
                                   'company_choices': all_companies})
    else:
        trips = Trip.objects \
            .filter(route__route_finish_name='Cuenca') \
            .order_by(*order_from_list)

        form = FilterForm(initial={'show_from': 'any',
                                   'show_to': 'Cuenca',
                               'company_choices': all_companies})

    return render(request, template,
                  {'trips': trips, 'form': form})


@csrf_exempt
def ajax_schedule_data(request):
    """ Transfer data with AJAX to the schedule table """
    if request.is_ajax():
        if request.method == 'POST':
            filter_list = []
            template = 'v2/includes/main_schedule_table.html'
            res_set = request.POST.copy()
            show_from = res_set['show_from']
            show_to = res_set['show_to']
            checkedCompanies = res_set['checkedCompanies'].split(',')
            company_list = [Q(route__company__name=company)
                                            for company in checkedCompanies]

            if show_from != 'any':
                filter_list.append(Q(route__route_start__name=show_from))

            if show_to != 'any':
                filter_list.append(Q(route__route_finish__name=show_to))

            if (show_to == 'any') & (show_from == 'any'):
                trips = Trip.objects \
                    .filter(reduce(operator.or_, company_list)) \
                    .order_by(*order_from_list)
            else:
                trips = Trip.objects \
                    .filter(reduce(operator.and_, filter_list)) \
                    .filter(reduce(operator.or_, company_list)) \
                    .order_by(*order_from_list)

            return render(request, template,
                          {'trips': trips})
