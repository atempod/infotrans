from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^schedule/$', views.schedule, name='schedule'),
    url(r'^ajax_schedule_data/', views.ajax_schedule_data,
                                            name='ajax_schedule_data'),
    url(r'^company_list/', views.CompanyListView.as_view(),
                                            name='company_list'),
    url(r'^company_details/(?P<pk>[0-9]+)/$', views.company_details,
                                            name='company_details'),
    url(r'^terminal_terrestre/', views.TerminalTerrestreView.as_view(),
                                            name='terminal_terrestre'),
    url(r'^routes_map/', views.routes_map, name='routes_map'),
    url(r'^feedback/$', views.feedback, name='feedback'),
    url(r'^about_project/', views.AboutProjectView.as_view(),
                                            name='about_project'),

    # url(r'^routes_map/', views.RoutesMapView.as_view(), name='routes_map'),
    # url(r'^schedule/(?P<direction>[a-z]*)/$',
    #                  views.schedule, name='schedule'),

    # url(r'^api/companies/$', api_views.get_post_companies, name='get_post_companies'),
    # url(r'^api/company_detail/(?P<pk>[0-9]+)$', api_views.get_delete_update_company, name='get_delete_update_company'),
    # url(r'^api/company_detail/(?P<pk>[0-9]+)$', api_views.get_company_details, name='get_company_details'),
]
