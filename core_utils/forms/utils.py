# -*- coding: utf-8 -*-

import json
from django import forms
from django.utils.encoding import force_text


def form_errors_to_json(form_list):
    if not isinstance(form_list, list):
        form_list = [form_list]
    errors = {}
    for f in form_list:
        if issubclass(f.__class__, forms.BaseFormSet):
            for ff in f.forms:
                errors.update(extract_errors(ff))
        else:
            errors.update(extract_errors(f))
    return json.dumps({'success': False, 'errors': errors})


def extract_errors(form):
    errors = {}
    for k, v in form.errors.items():
        field_name = k
        if form.prefix:
            field_name = u'%s-%s' % (form.prefix, k)
        errors[field_name] = force_text(v[0])
    # if hasattr(form, 'non_form_errors'):
    #     non_form_errors = form.non_form_errors()
    #     if non_form_errors:
    #         for nn, msg in enumerate(non_form_errors):
    #             key = 'non_form%s_error_%s' % (n, nn)
    #             errors[key] = force_text(msg)
    return errors
