from openpyxl import load_workbook


def excel2dict_list(excel_file, fields):
    """
    Reads worksheet of excel file with city data.
    Returns data as a list of dicts.
    """
    result_dict_list = []
    try:
        wb = load_workbook(excel_file, read_only=True)
        ws = wb.active
    except FileNotFoundError as e:
        print('File with excel data for Cities was not found', repr(e))
        return

    for row in range(2, ws.max_row + 1):
        entity_dict = {}
        if ws.cell(row, 1).value:
            pass
            for col in range (1, ws.max_column + 1):
                field_name = fields[col-1]
                entity_dict[field_name] = ws.cell(row, col).value
            result_dict_list.append(entity_dict)

    return result_dict_list
