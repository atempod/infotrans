from django.conf.urls import url, include
from django.contrib import admin

from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^v1', include('itrans.urls')),
    url(r'^v2/', RedirectView.as_view(url='/', permanent=False), name='index'),
    url(r'^', include('itrans.v2.urls')),
    url(r'^', include('reguser.urls')),
    url(r'^organizer/', include('organizer.urls')),
]
if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


