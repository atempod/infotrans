from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User


class SignUpForm(UserCreationForm):
    email = forms.EmailField(label='Email', max_length=254, help_text='Required. Type valid email address.')

    # username = forms.CharField(label='Nombre', max_length=32, help_text='Necesario.  No más de 32 signos. Solo letras, numeros y @/./+/-/_.')
    # email = forms.EmailField(label='Email', max_length=254, help_text='Necesario. El correo electrónico vigente.')
    # password1 = forms.CharField(label='Password', max_length=32, help_text='Necesario. No menos de 8 signos. No solo numeros.')
    # password2 = forms.CharField(label='Password confirmación', max_length=254, help_text='Necesario. Inform a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

