from django.conf.urls import url, include
from django.contrib import admin
from django.contrib import auth
# from django.contrib.auth.views imports login, logout
import django.contrib.auth.views as auth_views

from . import views

admin.autodiscover()


urlpatterns = [
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^login/$', auth_views.login, {'template_name': 'v2/reg_login/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/v2/'}, name='logout'),
    url(r'^account_activation_sent/$', views.account_activation_sent, name='account_activation_sent'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        views.activate, name='activate'),
    url(r'^password/$', views.change_password, name='change_password')
]
